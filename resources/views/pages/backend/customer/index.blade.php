@extends('layouts.backend')

@section('page-title') Customers @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Customers</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered file-export">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Profile Status</th>
                                    <th>Role</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                     @foreach($users as $serial => $user)
    
                                         <tr class="user_id_{{$user->user_id}}">
                                             <td>{{$user->sur_name}} {{$user->first_name}} {{$user->other_name}}</td>
                                             <td>{{$user->email}}</td>
                                             <td>{{$user->phone_number}}</td>
                                             <td>
                                                 @if($user->profile_complete_status == 0)
                                                     <p class="warning"><i class="la la-warning"></i> Incomplete</p>
                                                 @elseif($user->profile_complete_status == 0)
                                                     <p class="success"><i class="la la-success"></i> Complete</p>
                                                 @endif
                                             </td>
                                             <td>{{$user->name}}</td>
                                             <td>
                                                <a href="{{ url('customer/edit',['customer'=>$user->user_id]) }}"> <i class="la la-edit"></i> Edit </a>
                                                <a href="{{ url('customer/delete',['customer'=>$user->user_id]) }}"> <i class="la la-trash"></i> Delete </a>
                                             </td>
                                         </tr>
                                     @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

