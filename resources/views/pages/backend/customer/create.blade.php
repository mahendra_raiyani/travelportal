@extends('layouts.backend')

@section('page-title') New Travel Package @endsection

@section('content')

    <section class="flexbox-container">
	    <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="text-center">
                        <div class="card-body">
                            @if(empty($user->photo))
                                <img src="{{asset('backend/app-assets/images/logo/logo.png')}}" id="user_image" class="rounded-circle  height-150" alt="Card image">
                            @else
                                <img src="{{asset($user->photo)}}" class="rounded-circle  height-150" id="user_image" alt="Card image">
                            @endif
                        </div>
                        <div class="card-body">
                            @if(isset($user))
                            	<h4 class="card-title customer_full_name">{{$user->sur_name}} {{$user->first_name}} {{$user->other_name}}</h4>
							@endif
                        </div>
                    </div>
                </div>
    
                <!-- /End Photo & Description card -->
    
            </div>

	        <div class="col-md-9">
                @if($errors->any())
                    @foreach($errors->all() as $serial => $error)
                        <div class="alert round bg-danger alert-icon-left alert-arrow-left alert-dismissible mb-2" role="alert">
                            <span class="alert-icon"><i class="la la-thumbs-o-down"></i></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>Oh snap!</strong> {{$error}}
                        </div>
                    @endforeach
                @endif
                <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                    <div class="card-content">
                        <div class="card-body pt-0">
                            <form class="form-horizontal" method="post" action="@if(isset($user)) {{ action('CustomerController@update',['customer' => $user->user_id]) }} @else {{ action('CustomerController@store') }} @endif">
                                @csrf
                                <div class="row">
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Role *</lable>
                                            <select class="form-control" name="user_type" required>
                                                @foreach($roles as $serial => $roles)
                                                    <option value="{{$roles->id}}">{{$roles->name}}</option>
                                                @endforeach
                                            </select>
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Title *</lable>
                                            <select class="form-control" name="title_id" required>
                                             @foreach($titles as $serial => $title)
                                              <option value="{{$title->id}}">{{$title->name}}</option>
                                             @endforeach
                                            </select>
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Gender *</lable>
                                            <select class="form-control" name="gender_id" required>
                                                @foreach($genders as $serial => $gender)
                                                    <option value="{{$gender->id}}">{{$gender->type}}</option>
                                                @endforeach
                                            </select>
			                            </div>
			                        </div>
                                </div>
                                <div class="row">
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Sur Name *</lable>
			                                <input type="text" class="form-control" name="sur_name" value="{{ old('sur_name', ( isset($user) ? $user->sur_name : '') ) }}" required placeholder="e.g (Basset)" />
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>First Name *</lable>
			                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name', ( isset($user) ? $user->first_name : '') ) }}" required placeholder="e.g (Martin)" />
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Other Name *</lable>
			                                <input type="text" class="form-control" name="other_name" value="{{ old('other_name', ( isset($user) ? $user->other_name : '') ) }}" required placeholder="e.g (Peter)" />
			                            </div>
			                        </div>
                                </div>
                                <div class="row">
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Email *</lable>
			                                <input type="text" class="form-control" name="email" value="{{ old('email', ( isset($user) ? $user->email : '') ) }}" required placeholder="Valid Email" />
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Phone *</lable>
			                                <input type="text" class="form-control" name="phone" value="{{ old('phone', ( isset($user) ? $user->phone_number : '') ) }}" required placeholder="Phone Number (08101010101)" />
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Password *</lable>
			                                <input type="text" class="form-control" name="password" value="{{ old('password', ( isset($user) ? $user->actualpassword : '') ) }}" required placeholder="Password" />
			                            </div>
			                        </div>
                                </div>
                                <div class="row">
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Address *</lable>
                                            <textarea class="form-control" name="address" required>{{ old('address', ( isset($user) ? $user->address : '') ) }}</textarea>
			                            </div>
			                        </div>
			                        <div class="col-md-4">
			                            <div class="form-group">
			                                <lable>Image </lable>
			                                <input type="file" class="form-control" name="photo"  />
			                            </div>
			                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-8 col-md-10"></div>
                                    <div class="col-12 col-sm-2 col-md-2">
                                        <button type="submit" class="btn btn-info btn-block"><i class="ft-user"></i> Create</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


