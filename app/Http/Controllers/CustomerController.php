<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use nilsenj\Toastr\Facades\Toastr;
use App\Gender;
use App\Title;
use App\Role;
use App\User;
use App\Profile;
use App\RoleUser;


class CustomerController extends Controller
{

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$users = User::join('profiles','profiles.user_id','=','users.id')
    	->join('role_user','role_user.user_id','=','users.id')
    	->join('roles','role_user.role_id','=','roles.id')
    	->where('role_user.role_id','3')
    	->where('users.delete_status','0')
    	->get();

    	return view('pages.backend.customer.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('pages.backend.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$request->validate([
    					'sur_name'   => 'required|string|max:255',
    					'first_name' => 'required|string|max:255',
    					'other_name' => 'required|string|max:255',
    					'email'      => 'required|string|email|max:255|unique:users',
    					'phone'      => 'required',
    					'password'   => 'required|string|min:6',
    	]);
    	
    	
    	$user = User::create([
    					'email' => $request['email'],
    					'password' => Hash::make($request['password']),
    					'actualpassword' => $request['password'],
    	]);
    	
    	$user->attachRole(3);
    	
    	$profile = Profile::create([
    					'user_id'       => $user->id,
    					'title_id'      => $request['title_id'],
    					'gender_id'     => $request['gender_id'],
    					'sur_name'      => $request['sur_name'],
    					'first_name'    => $request['first_name'],
    					'other_name'    => array_get($request,'first_name',''),
    					'phone_number'  => $request['phone'],
    					'address'       => $request['address'],
    					'photo'         => array_get($request,'photo',''),
    	]);
    	
    	if($profile AND $user){
    		Toastr::success('New users created successfully');
    	}
    	else{
    		Toastr::error('Unable to create new user');
    	}
    	
    	return redirect()->action('CustomerController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $user = User::join('profiles','profiles.user_id','=','users.id')       
       ->where('users.id',$id)
       ->join('role_user','role_user.user_id','=','users.id')
       ->join('roles','role_user.role_id','=','roles.id')
       ->where('role_user.role_id','3')
       ->first();

       $titles  = Title::all();
       $genders = Gender::all();
       $roles   = Role::all();
       
//      echo "<pre>";
//      print_r($user);
//      exit;
       
       return view('pages.backend.customer.create',compact('user','titles','genders','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'sur_name'   => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'other_name' => 'required|string|max:255',
            'phone'      => 'required',
            'password'   => 'required|string|min:6',
        ]);
     
        $user = User::find($id);
        
        if($request->email != $user->email){
         $this->validate($request, [
             'email'      => 'required|string|email|max:255|unique:users',
         ]);
        }
        
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->actualpassword = $request->password;
        
        
        $updateUser = $user->update();
        
        $profile = Profile::where('user_id',$id)->first();
        $profile->title_id     = $request->title_id;
        $profile->gender_id    = $request->gender_id;
        $profile->sur_name     = $request->sur_name;
        $profile->first_name   = $request->first_name;
        $profile->other_name   = $request->other_name;
        $profile->phone_number = $request->phone;
        $profile->address      = $request->address;

        $file      = $request->file('photo');
        
        if($file){
           $fileName  = time().$file->getClientOriginalName();
           $file_path = 'images/users/profile/'.$fileName;
           $file->move(public_path('/images/users/profile/'),$fileName);
           $profile = Profile::where('user_id',$id)->first();
           $profile->photo = $file_path;
        }
        
        $updateProfile = $profile->update();
        $userRole = RoleUser::where('user_id',$id)->first()->role_id;
        
        if($userRole != $request->user_type){
         $user->detachRole($userRole);
         $user->attachRole($request->user_type);
        }
        
        if($updateUser AND $updateProfile){
         Toastr::success('User information updated successfully');
        }
        else{
         Toastr::error('Unable to edit user information');
        }

        return redirect()->action('CustomerController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$user = User::find($id);
    	$user->delete_status = 1;
    	$user->update();
    	return redirect()->action('CustomerController@index');
    }
}
